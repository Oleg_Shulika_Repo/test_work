<?php

require_once("model.php");

class Main
{
    private $films;

    function getData() {

        return $this->films = (new Model)->getFilms();

    }

    function render() {

        $films = $this->getData();

        extract(array($films));

        ob_start();
        include('views/all_films.php');
        $renderedView = ob_get_clean();

        echo $renderedView;

    }

    function renderAddFilm() {

        ob_start();
        include('views/add_film.php');
        $renderedView = ob_get_clean();

        echo $renderedView;

    }
}

if(substr($_SERVER[REQUEST_URI], 1) == 'add_film') {
    (new Main)->renderAddFilm();
}
else {
    (new Main)->render();
}

