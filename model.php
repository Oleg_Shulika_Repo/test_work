<?php

class Model
{

    private $serverName = "localhost";
    private $userName = "root";
    private $password = "root";
    private $tableName = "test_db";

    function getConnection() {
        $con = mysqli_connect($this->serverName, $this->userName, $this->password, $this->tableName);

        if (mysqli_connect_errno()) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
            die;
        }

        return $con;
    }

    function getFilms() {
        $connection = $this->getConnection();
        $sql = 'SELECT name, year FROM Films WHERE isActive = 1';
        $result = mysqli_query($connection, $sql);

        return $result;
    }

    function validation() {
        if (!preg_match('/^[a-zA-Z0-9 .\-]+$/i', $_POST["name"]) || empty($_POST["name"])) {
            $errorMessage = 'Invalid or empty Name';
            $this->setError($errorMessage);
        }
        if(!preg_match('/^[0-9.\-]+$/i', $_POST["year"]) || empty($_POST["year"])) {
            $errorMessage = 'Invalid or empty Year';
            $this->setError($errorMessage);
        }

        $this->addFilm($_POST["name"], $_POST["year"]);
    }

    function setError($message) {
        $error = array('error' => $message);
        echo json_encode($error);
        die;
    }

    function addFilm($name, $year) {
        $connection = $this->getConnection();
        $sql = sprintf('INSERT INTO Films (name, year) VALUES ("%s", "%d")', $name, $year);
        mysqli_query($connection, $sql);

        $error = array('success' => 'Film is added');
        echo json_encode($error);
        die;
    }
}

if($_POST) {
    (new Model)->validation();
}