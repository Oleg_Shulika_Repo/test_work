<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="/assets/js/test.js"></script>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <form>
                <div class="form-group">
                    <label for="name">Film Name</label>
                    <input type="text" class="form-control" id="name" placeholder="Film Name" name="name">
                </div>
                <div class="form-group">
                    <label for="year">Film Year</label>
                    <input type="number" class="form-control" id="year" placeholder="Film Year" name="year">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            <div class="text-danger"></div>
            <div class="text-success"></div>
        </div>
    </div>
</div>