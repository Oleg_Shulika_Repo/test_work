<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <table class="table table-striped col-md-1">
                <tr>
                    <td>Name</td>
                    <td>Year</td>
                </tr>
                <?php
                while ($row = mysqli_fetch_assoc($films)) {
                    echo '<tr>';
                    echo '<td>'.$row['name'].'</td>';
                    echo '<td>'.$row['year'].'</td>';
                    echo '</tr>';
                }
                ?>
            </table>
            <a href="/add_film" >Add Film</a>
        </div>
    </div>
</div>