jQuery(document).ready(function($){
    $('form').on('submit',function() {
        $.ajax( {
            url: "model.php",
            type: "POST",
            data: $( "form" ).serialize(),
            success: function( response ) {
                var data = JSON.parse(response);
                if(data.error) {
                    $('.text-success').text('');
                    $('.text-danger').text(data.error);
                }
                else {
                    $('.text-danger').text('');
                    $('.text-success').text(data.success);
                    location.reload();
                }
            }
        });

        return false;

    })
})